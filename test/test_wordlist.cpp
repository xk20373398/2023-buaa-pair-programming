#include <iostream>
#include "gtest/gtest.h"

int gen_chains_all(char *words[], int len, char *result[]);

int read_file(char *words[], std::string file_path);

int gen_chain_word(char* words[], int len, char* result[], char head, char tail, char reject, bool enable_loop);

int gen_chain_char(char* words[], int len, char* result[], char head, char tail, char reject, bool enable_loop);

char *words1[100];
char *words2[200];
char *words3[100];
char *words4[100];
char *words5[100];
char *words6[100];
char *words7[100];
char *words8[100];
char *words9[100];
char *words10[100];

TEST(read_file, r_f1) {
    EXPECT_EQ(20, read_file(words1, "word1.txt"));
}

TEST(read_file, r_f2) {
    EXPECT_EQ(20, read_file(words2, "word2.txt"));
}

TEST(gen_chains_all, gca1) {
    char *result[100];
    read_file(words3, "word3.txt");
    EXPECT_EQ(48, gen_chains_all(words3, 10, result));
}

TEST(gen_chains_all, gca2) {
    char *result[2000];
    read_file(words4, "word4.txt");
    EXPECT_EQ(1837, gen_chains_all(words4, 10, result));
}

TEST(gen_chain_word, gcw1) {
    char *result[100];
    read_file(words5, "word5.txt");
    EXPECT_EQ(10, gen_chain_word(words5, 10, result, 'n', 'h', 'o', false));
}

TEST(gen_chain_word, gcw2) {
    char *result[100];
    read_file(words6, "word6.txt");
    EXPECT_EQ(8, gen_chain_word(words6, 10, result, 's', 'x', 't', true));
}

TEST(gen_chain_word, gcw3) {
    char *result[100];
    read_file(words7, "word7.txt");
    EXPECT_EQ(10, gen_chain_word(words7, 10, result, 'z', 'd', 0, false));
}

TEST(gen_chain_char, gcc1) {
    char *result[100];
    read_file(words8, "word8.txt");
    EXPECT_EQ(20, gen_chain_char(words8, 20, result, 'z', 'x', 'h', true));
}


TEST(gen_chain_char, gcc2) {
    char *result[100];
    read_file(words9, "word9.txt");
    EXPECT_EQ(20, gen_chain_char(words9, 20, result, 'n', 'z', 0, false));
}



TEST(gen_chain_char, gcc3) {
    char *result[100];
    read_file(words10, "word10.txt");
    EXPECT_EQ(51, gen_chain_char(words10, 20, result, 'z', 'u', 'h', true));
}

int main() {
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}