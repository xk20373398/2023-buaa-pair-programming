//
// Created by user_xk on 2023/3/8.
//

#ifndef MAX_LENGTH_CHAIN_SOLUTION_H
#define MAX_LENGTH_CHAIN_SOLUTION_H

#include<vector>
#include <string>
#include "set"
#include "queue"

using namespace std;


typedef struct vertex {
    set<int> in_degrees;
    set<int> out_degrees;
} Vertex;

Vertex vertexes[26];

struct cmp_words {
    bool operator() (string word1, string word2) {
        return word1.length() < word2.length();
    }
};

struct edge {
    vector<string> words;
    priority_queue<string, vector<string>, cmp_words> words_priority_queue;
    int num;
    bool is_free;
//    string max_length_word;
};

typedef struct edge Edge;

Edge graph[26][26];

struct record {
    vector<int> nodes;
};

struct cmp_record {
    bool operator() (record record1, record record2) {
        return record1.nodes < record2.nodes;
    }
};

typedef struct record Record;


string solution(FILE* file, int type, char head_letter, char tail_letter, char head_not_letter, bool is_circle);

int init_graph(FILE* file);
// 对于h, t, j，输入NULL代表无限制
string get_all_chain(char head_letter, char tail_letter, char head_not_letter, bool is_circle);
string get_max_length_chain(bool is_word, char head_letter, char tail_letter, char head_not_letter, bool is_circle);


#endif //MAX_LENGTH_CHAIN_SOLUTION_H
