//
// Created by user_xk on 2023/3/15.
//

#ifndef MAX_LENGTH_CHAIN_DUPLICATEDWORDEXCEPTION_H
#define MAX_LENGTH_CHAIN_DUPLICATEDWORDEXCEPTION_H

#include "stdexcept"
using namespace std;

class DuplicatedWordException : public logic_error {
public:
    DuplicatedWordException(string word): logic_error("have duplicated word: ") {
        this->word = word;
    }

    string print_exception() {
        return this->what() + word;
    }

private:
    string word;
};


#endif //MAX_LENGTH_CHAIN_DUPLICATEDWORDEXCEPTION_H
