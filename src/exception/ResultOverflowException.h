//
// Created by user_xk on 2023/3/15.
//

#ifndef CORE_DLL_RESULTOVERFLOWEXCEPTION_H
#define CORE_DLL_RESULTOVERFLOWEXCEPTION_H

#include "stdexcept"
using namespace std;

class ResultOverflowException : public logic_error {
public:
    ResultOverflowException(): logic_error("result chains more than 20000!") {

    }

    string print_exception() {
        return this->what();
    }
};


#endif //CORE_DLL_RESULTOVERFLOWEXCEPTION_H
