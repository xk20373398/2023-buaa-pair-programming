//
// Created by user_xk on 2023/3/13.
//

#ifndef MAX_LENGTH_CHAIN_CIRCLETYPEEXCEPTION_H
#define MAX_LENGTH_CHAIN_CIRCLETYPEEXCEPTION_H

#include "stdexcept"
using namespace std;

class CircleTypeException : public logic_error {
public:
    CircleTypeException(bool is_circle): logic_error("") {
        this->is_circle = is_circle;
    }

    string print_exception() {
        if (this->is_circle) {
            return "lack the parameter -r!";
        }
        else {
            return "parameter - r is not required!";
        }
    }

private:
    bool is_circle;
};


#endif //MAX_LENGTH_CHAIN_CIRCLETYPEEXCEPTION_H
