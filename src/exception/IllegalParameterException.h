//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_ILLEGALPARAMETEREXCEPTION_H
#define MAX_LENGTH_CHAIN_ILLEGALPARAMETEREXCEPTION_H

#include "stdexcept"
using namespace std;

class IllegalParameterException : public logic_error {
public:
    IllegalParameterException(): logic_error("illegal parameter occurred!") {

    }

    string print_exception() {
        return this->what();
    }

};


#endif //MAX_LENGTH_CHAIN_ILLEGALPARAMETEREXCEPTION_H
