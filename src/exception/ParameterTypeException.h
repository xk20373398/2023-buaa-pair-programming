//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_PARAMETERTYPEEXCEPTION_H
#define MAX_LENGTH_CHAIN_PARAMETERTYPEEXCEPTION_H

#include "stdexcept"
using namespace std;

class ParameterTypeException : public logic_error {
public:
    ParameterTypeException(string after_param): logic_error("") {
        this->after_param = after_param;
    }

    string print_exception() {
        return "the parameter after " + this->after_param + " should be a letter but not a string!";
    }

private:
    string after_param = "";
};


#endif //MAX_LENGTH_CHAIN_PARAMETERTYPEEXCEPTION_H
