//
// Created by user_xk on 2023/3/15.
//

#ifndef MAX_LENGTH_CHAIN_WORDSOVERFLOWEXCEPTION_H
#define MAX_LENGTH_CHAIN_WORDSOVERFLOWEXCEPTION_H

#include "stdexcept"
using namespace std;

class WordsOverflowException : public logic_error {
public:
    WordsOverflowException(bool is_circle): logic_error("") {
        this->is_circle = is_circle;
    }

    string print_exception() {
        if (is_circle) {
            return "words more than 100!";
        }
        else {
            return "words more than 10000!";
        }
    }

private:
    bool is_circle;
};


#endif //MAX_LENGTH_CHAIN_WORDSOVERFLOWEXCEPTION_H
