//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_FILECONTENTEXCEPTION_H
#define MAX_LENGTH_CHAIN_FILECONTENTEXCEPTION_H

#include <utility>

#include "stdexcept"
using namespace std;

class FileContentException : public logic_error {
public:
    explicit FileContentException(string c): logic_error("file contains unexpected character: ") {
        this->c = std::move(c);
    }

    string print_exception() {
        string s = this->what();
        s += c;
        return s;
    }

private:
    string c;
};


#endif //MAX_LENGTH_CHAIN_FILECONTENTEXCEPTION_H
