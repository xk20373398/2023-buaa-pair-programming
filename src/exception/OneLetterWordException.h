//
// Created by user_xk on 2023/3/15.
//

#ifndef MAX_LENGTH_CHAIN_ONELETTERWORDEXCEPTION_H
#define MAX_LENGTH_CHAIN_ONELETTERWORDEXCEPTION_H

#include "stdexcept"
using namespace std;

class OneLetterWordException : public logic_error {
public:
    OneLetterWordException(string word): logic_error("have one-letter word: ") {
        this->word = word;
    }

    string print_exception() {
        return this->what() + word;
    }

private:
    string word;
};


#endif //MAX_LENGTH_CHAIN_ONELETTERWORDEXCEPTION_H
