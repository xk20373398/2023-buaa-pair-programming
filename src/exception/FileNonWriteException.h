//
// Created by user_xk on 2023/3/16.
//

#ifndef LIB_DLL_FILENOTWRITEEXCEPTION_H
#define LIB_DLL_FILENOTWRITEEXCEPTION_H

#include "stdexcept"
using namespace std;

class FileNonWriteException : public logic_error {
public:
    FileNonWriteException(string file_name): logic_error(" is not writable!") {
        this->file_name = file_name;
    }


    string print_exception() {
        return file_name + this->what();
    }

private:
    string file_name;
};


#endif //LIB_DLL_FILENOTWRITEEXCEPTION_H
