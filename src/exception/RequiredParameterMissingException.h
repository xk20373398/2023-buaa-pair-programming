//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_REQUIREDPARAMETERMISSINGEXCEPTION_H
#define MAX_LENGTH_CHAIN_REQUIREDPARAMETERMISSINGEXCEPTION_H

#include "stdexcept"
using namespace std;

class RequiredParameterMissingException : public logic_error {
public:
    RequiredParameterMissingException(string after_param): logic_error("missing required parameter after ") {
        this->after_param = after_param;
    }

    RequiredParameterMissingException(): logic_error("missing required parameter") {

    }

    string print_exception() {
        return this->what() + after_param + "!";
    }

private:
    string after_param = "";
};


#endif //MAX_LENGTH_CHAIN_REQUIREDPARAMETERMISSINGEXCEPTION_H
