//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_CONFLICTPARAMETEREXCEPTION_H
#define MAX_LENGTH_CHAIN_CONFLICTPARAMETEREXCEPTION_H

#include "stdexcept"
using namespace std;

class ConflictParameterException : public logic_error {
public:
    ConflictParameterException(): logic_error("parameters not compatible!") {

    }

    string print_exception() {
        return this->what();
    }

};


#endif //MAX_LENGTH_CHAIN_CONFLICTPARAMETEREXCEPTION_H
