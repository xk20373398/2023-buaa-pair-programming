//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_FILEEMPTYEXCEPTION_H
#define MAX_LENGTH_CHAIN_FILEEMPTYEXCEPTION_H


#include "stdexcept"
using namespace std;

class FileEmptyException : public logic_error {
public:
    FileEmptyException(): logic_error("words file is empty!") {

    }

    string print_exception() {
        return this->what() ;
    }

};


#endif //MAX_LENGTH_CHAIN_FILEEMPTYEXCEPTION_H
