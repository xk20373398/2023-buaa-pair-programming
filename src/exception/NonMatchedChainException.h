//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_NONMACTEDCHAIN_H
#define MAX_LENGTH_CHAIN_NONMACTEDCHAIN_H

#include "stdexcept"
using namespace std;

class NonMatchedChainException : public logic_error {
public:
    NonMatchedChainException(): logic_error("not have matched chain!") {

    }

    string print_exception() {
        return this->what();
    }

};



#endif //MAX_LENGTH_CHAIN_NONMACTEDCHAIN_H
