//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_FILENONREADEXCEPTION_H
#define MAX_LENGTH_CHAIN_FILENONREADEXCEPTION_H

#include "stdexcept"
using namespace std;

class FileNonReadException : public logic_error {
public:
    FileNonReadException(string file_name): logic_error(" is not readable!") {
        this->file_name = file_name;
    }

    string print_exception() {
        return file_name + this->what();
    }

private:
    string file_name;
};


#endif //MAX_LENGTH_CHAIN_FILENONREADEXCEPTION_H
