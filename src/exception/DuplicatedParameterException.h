//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_DUPLICATEDPARAMETEREXCEPTION_H
#define MAX_LENGTH_CHAIN_DUPLICATEDPARAMETEREXCEPTION_H

#include "stdexcept"
using namespace std;

class DuplicatedParameterException : public logic_error {
public:
    DuplicatedParameterException(): logic_error("multiple occurrences of the same parameter!") {

    }

    string print_exception() {
        return this->what();
    }

};


#endif //MAX_LENGTH_CHAIN_DUPLICATEDPARAMETEREXCEPTION_H
