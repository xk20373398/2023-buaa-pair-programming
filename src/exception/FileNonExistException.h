//
// Created by user_xk on 2023/3/11.
//

#ifndef MAX_LENGTH_CHAIN_FILENONEXISTEXCEPTION_H
#define MAX_LENGTH_CHAIN_FILENONEXISTEXCEPTION_H

#include "stdexcept"
using namespace std;

class FileNonExistException : public logic_error {
public:
    FileNonExistException(string file_name): logic_error(" is non-existent!") {
        this->file_name = file_name;
    }

    string print_exception() {
        return file_name + this->what();
    }

private:
    string file_name;
};


#endif //MAX_LENGTH_CHAIN_FILENONEXISTEXCEPTION_H
